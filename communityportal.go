package main

import (
	"encoding/json"
	"fmt"

	"github.com/startupweekend/communityportal/providers"
	"github.com/startupweekend/communityportal/providers/angellist"
	"github.com/startupweekend/communityportal/providers/swoop"
	"github.com/startupweekend/communityportal/providers/twitter"
)

func main() {
	sources := make(map[string]providers.Provider)

	sources["swoop_applications"] = swoop.New(
		"events",
		map[string]string{
			"event_type":   "startup weekend",
			"event_status": "W|G",
			"since":        "2015-01-01",
		},
	)

	sources["events_this_year"] = swoop.New(
		"events",
		map[string]string{
			"event_type":   "startup weekend",
			"event_status": "N",
			"since":        "2015-05-01",
			"until":        "2015-05-31",
		},
	)

	sources["angellist_startups"] = angellist.New("seattle", "Startup")
	sources["angellist_people"] = angellist.New("seattle", "User")

	terms := []string{"seattle", "coworking", "entrepreneurship"}
	sources["twitter_search"] = twitter.New(terms)

	results := providers.RunAll(sources)
	output, _ := json.Marshal(results)
	fmt.Println(string(output))
}
