package angellist

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"github.com/startupweekend/communityportal/providers"
)

// See https://angel.co/api/spec/search

const (
	alProtocol string = "https"
	alHost     string = "api.angel.co"
)

// Angellist - A provider for Angellist searches
type Angellist struct {
	providers.Base
	Query string
	Type  string
}

// New - Returns a new Angellist search object
func New(query string, searchType string) (al Angellist) {
	al = Angellist{Query: query, Type: searchType}
	al.RemoteHandler = providers.CallRemote

	return
}

// Checks the Angellist instance to make sure the client
// is asking for a valid Search Type
// Can be one of User, Startup, MarketTag or LocationTag.
func (p *Angellist) resourceOk() bool {
	return p.Type == "User" || p.Type == "Startup" ||
		p.Type == "MarketTag" || p.Type == "LocationTag"
}

// URL Returns resource URL for the Angellist search
func (p Angellist) URL() (resourceURL string, err error) {
	if !(p.resourceOk()) {
		err = errors.New("Invalid search type")
		return
	}

	v := url.Values{}
	v.Add("query", p.Query)
	v.Add("type", p.Type)

	urlObj := url.URL{
		Scheme:   alProtocol,
		Host:     alHost,
		Path:     "1/search",
		RawQuery: v.Encode(),
	}

	resourceURL = urlObj.String()

	return
}

// Get Returns the results of an Angellist search
func (p Angellist) Get() (result providers.Result) {
	result = providers.Result{Success: false, ResourceURL: "", Data: nil}

	searchURL, err := p.URL()
	if err != nil {
		fmt.Println("Error generating Angellist URL")
		fmt.Println(err)
		return
	}

	body, err := p.RemoteHandler(searchURL, nil)
	if err != nil {
		fmt.Println("Error on reading body")
		fmt.Println(err)
		return
	}

	// Parse Startups response
	var jsonData []angellistResponse
	jsonErr := json.Unmarshal(body, &jsonData)

	if jsonErr != nil {
		fmt.Println("Error parsing JSON")
		fmt.Println(jsonErr)
		return
	}

	result.Data = jsonData
	result.ResourceURL = searchURL
	result.Success = true

	return
}
