package angellist

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func urlGoodTest(t *testing.T, al Angellist, expected string) {
	searchURL, err := al.URL()
	if err != nil {
		t.Errorf("Did not expect an error. Got %s\n", err.Error())
	}

	if searchURL != expected {
		t.Errorf("Unexpected URL. Got %s\n", searchURL)
	}
}

func TestURL(t *testing.T) {
	Convey("Generating a URL", t, func() {

		Convey("When given a User type", func() {
			al := New("test", "User")
			searchURL, err := al.URL()
			Convey("It should generate the right URL without error", func() {
				So(err, ShouldBeNil)
				So(searchURL, ShouldEqual, "https://api.angel.co/1/search?query=test&type=User")
			})
		})

		Convey("When given a Startup type", func() {
			al := New("test", "Startup")
			searchURL, err := al.URL()
			Convey("It should generate the right URL without error", func() {
				So(err, ShouldBeNil)
				So(searchURL, ShouldEqual, "https://api.angel.co/1/search?query=test&type=Startup")
			})
		})

		Convey("When given a MarketTag type", func() {
			al := New("test", "MarketTag")
			searchURL, err := al.URL()
			Convey("It should generate the right URL without error", func() {
				So(err, ShouldBeNil)
				So(searchURL, ShouldEqual, "https://api.angel.co/1/search?query=test&type=MarketTag")
			})
		})

		Convey("When given a Location type", func() {
			al := New("test", "LocationTag")
			searchURL, err := al.URL()

			Convey("It should generate the right URL without error", func() {
				So(err, ShouldBeNil)
				So(searchURL, ShouldEqual, "https://api.angel.co/1/search?query=test&type=LocationTag")
			})
		})

		Convey("When given an invalid type", func() {
			al := New("", "Investors")
			_, err := al.URL()
			Convey("It should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})

	})
}

func makeHandler(data []byte) func(string, map[string]string) ([]byte, error) {
	return func(url string, headers map[string]string) ([]byte, error) {
		return data, nil
	}
}
func TestGet(t *testing.T) {
	Convey("Getting search results", t, func() {
		Convey("When given a good search", func() {
			al := New("Seattle", "Startup")
			al.RemoteHandler = makeHandler([]byte(`[
		  {
		    "id" : 1234,
		    "name" : "Startup Weekend",
		    "pic" : "http://www.something.com/pic.png",
		    "type" : "Startup",
		    "url" : "http://www.startupweekend.org"
		  },
		  {
		    "id" : 4321,
		    "name" : "UP Global",
		    "pic" : "http://www.something.com/photo.jpg",
		    "type" : "Startup",
		    "url" : "http://www.up.co"
		  }
		]`))

			result := al.Get()

			Convey("It should return a successful URL", func() {
				So(result.Success, ShouldBeTrue)
				So(result.ResourceURL, ShouldEqual, "https://api.angel.co/1/search?query=Seattle&type=Startup")
			})

			data := result.Data.([]angellistResponse)
			Convey("It should convert the response to Angellist data", func() {
				So(result.Data, ShouldNotBeNil)
				So(len(data), ShouldEqual, 2)
				So(data[0].ID, ShouldEqual, 1234)
			})

		})

		Convey("When the search is misconfigured", func() {
			Convey("It should not call the remote handler", func() {
				al := New("", "Investor")
				wasCalled := false
				al.RemoteHandler = func(url string, headers map[string]string) ([]byte, error) {
					wasCalled = true // shouldn't get called!
					return nil, nil
				}

				result := al.Get()
				So(result.Success, ShouldBeFalse)
				So(wasCalled, ShouldBeFalse)
			})
		})
	})
}
