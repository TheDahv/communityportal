package angellist

type angellistResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Pic  string `json:"pic"`
	Type string `json:"type"`
	URL  string `json:"url"`
}
