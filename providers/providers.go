package providers

// NOTES
// Other ideas for mapping a community
// * http://fi.co/posts/13401

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// Result represents the result of a resource call
type Result struct {
	Success     bool
	ResourceURL string
	Data        interface{}
}

// Base is data that any provider would need to provide
type Base struct {
	Query         map[string]string
	RemoteHandler func(remoteURL string, headers map[string]string) ([]byte, error)
}

// CallRemote Calls the remote data source with the query defined in the
// provider configuration
func CallRemote(remoteURL string, headers map[string]string) (body []byte, err error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", remoteURL, nil)

	if err != nil {
		fmt.Println("Error creating request")
		fmt.Println(err)
		return
	}

	for name, value := range headers {
		req.Header.Add(name, value)
	}

	// Get remote response
	response, err := client.Do(req)
	defer response.Body.Close()

	// Read body and return results
	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading body")
		fmt.Println(err)
		return
	}

	err = nil
	return
}

// Provider returns JSON data from a remote web data source
type Provider interface {
	Get() Result
	URL() (string, error)
}

type providerSources map[string]Provider
type providerOutput struct {
	Key    string
	Result Result
}

// RunAll Given a set of providers and keys under which to store each
// result, return a map of only the successful results
func RunAll(sources providerSources) (output map[string]interface{}) {
	output = make(map[string]interface{})
	resultsChan := make(chan providerOutput)

	for key, provider := range sources {
		go func(key string, provider Provider) {
			// Skip if we're not able to build a URL
			if _, urlErr := provider.URL(); urlErr == nil {
				result := provider.Get()
				resultsChan <- providerOutput{key, result}
			} else {
				fmt.Println("Fail path")
				fmt.Println(urlErr)
				resultsChan <- providerOutput{Key: "", Result: Result{Success: false}}
			}
		}(key, provider)
	}

	for i := 0; i < len(sources); i++ {
		response := <-resultsChan
		if response.Result.Success {
			output[response.Key] = response.Result.Data
		}
	}
	close(resultsChan)

	return
}
