package swoop

type embeddedperson struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Twitter   string `json:"twitter_handle"`
}

type venue struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

type swoopevent struct {
	// Event information
	ID        int    `json:"id"`
	City      string `json:"city"`
	State     string `json:"state"`
	Country   string `json:"country"`
	StartDate string `json:"start_date"`
	Edition   string `json:"vertical"`

	// People
	Facilitator embeddedperson   `json:"facilitator"`
	Organizers  []embeddedperson `json:"organizers"`
	Coaches     []embeddedperson `json:"coaches"`
	Judges      []embeddedperson `json:"judges"`

	// Other Details
	Website string `json:"website"`
	Venue   venue  `json:"venue"`
}
