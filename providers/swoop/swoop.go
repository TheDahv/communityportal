package swoop

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"github.com/startupweekend/communityportal/providers"
)

// TODO: Make these consts configurable based on the environment
// so we can write tests or run against dev environments
const (
	swoopProtocol string = "https"
	swoopHost     string = "swoop.up.co"
)

// Swoop A provider for the SWOOP data source
// Create it with the following before calling Get to call SWOOP
//    - Resource - The SWOOP resource to call. Can be 'events', 'people',
//      or 'community'
//    - Query - A map of query parameters
type Swoop struct {
	providers.Base
	Resource string
}

// New Returns a new Swoop object given a resource and query
func New(resource string, query map[string]string) (swoop *Swoop) {
	swoop = new(Swoop)

	swoop.Resource = resource
	swoop.Query = query
	swoop.RemoteHandler = providers.CallRemote

	return
}

// Checks a SWOOP instance to see if it is set up with the proper resource
func (p *Swoop) resourceOk() bool {
	// TODO Add support for "people" and "communities"
	return p.Resource == "events"
}

// URL Returns resource URL for this SWOOP Resource object
func (p Swoop) URL() (resourceURL string, err error) {
	if !(p.resourceOk()) {
		err = errors.New("Invalid resource")
		return
	}

	// Build up the query string
	v := url.Values{}
	for key, value := range p.Query {
		v.Add(key, value)
	}

	// Our final call URL
	urlObj := url.URL{
		Scheme:   swoopProtocol,
		Host:     swoopHost,
		Path:     p.Resource,
		RawQuery: v.Encode(),
	}

	resourceURL = urlObj.String()

	return
}

// Get - Given a SWOOP object preconfigured with the desired query,
// call SWOOP
func (p Swoop) Get() (result providers.Result) {
	result = providers.Result{Success: false, ResourceURL: "", Data: nil}

	searchURL, err := p.URL()
	if err != nil {
		fmt.Println("Error creating SWOOP URL")
		fmt.Println(err)
		return
	}

	body, err := p.RemoteHandler(searchURL, nil)
	if err != nil {
		fmt.Println("Error on Reading body")
		fmt.Println(err)
		return
	}

	// TODO Write parser code for each resource type
	jsonData := []swoopevent{}
	jsonErr := json.Unmarshal(body, &jsonData)

	if jsonErr != nil {
		fmt.Println("Error parsing JSON")
		fmt.Println(jsonErr)
		return
	}

	result.Success = true
	result.ResourceURL = searchURL
	result.Data = jsonData

	return
}
