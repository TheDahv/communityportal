package swoop

import (
	"net/url"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestURL(t *testing.T) {
	Convey("Generating a URL", t, func() {
		Convey("When given a good resource", func() {
			swoop := New("events", nil)
			url, err := swoop.URL()

			Convey("It should generate the right URL without error", func() {
				So(err, ShouldBeNil)
				So(url, ShouldEqual, "https://swoop.up.co/events")
			})
		})

		Convey("When given a bad resource", func() {
			swoop := New("badgers", nil)
			url, err := swoop.URL()

			Convey("It should generate an empty URL", func() {
				So(url, ShouldEqual, "")
			})

			Convey("It should return the appropriate error", func() {
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldEqual, "Invalid resource")
			})
		})

		Convey("When given an event query", func() {

			swoop := New("events", map[string]string{
				"event_status": "W|G",
				"event_type":   "startup week$",
			})

			swoopURL, err := swoop.URL()

			expected := "https://swoop.up.co/events?event_status=" +
				url.QueryEscape("W|G") +
				"&event_type=" +
				url.QueryEscape("startup week$")

			Convey("It should generate the right URL without error", func() {
				So(swoopURL, ShouldEqual, expected)
				So(err, ShouldBeNil)
			})
		})
	})
}

func makeHandler(data []byte) func(string, map[string]string) ([]byte, error) {
	return func(url string, headers map[string]string) ([]byte, error) {
		return data, nil
	}
}

func TestGet(t *testing.T) {
	Convey("Returning a Provider Response", t, func() {
		Convey("When given a simple event search", func() {
			swoop := New("events", nil)
			swoop.RemoteHandler = makeHandler([]byte(`[
		  {
		    "id" : 123,
		    "start_date" : "2015-01-01",
		    "city" : "Seattle",
		    "state" : "WA",
		    "country" : "USA",
		    "vertical" : "",
		    "organizers" : [
		      {
		        "first_name" : "Test",
		        "last_name" : "Organizer",
		        "twitter_handle" : "testorganizer"
		      }
		    ],
		    "website" : "http://swoop.up.co",
		    "venue" : {
		      "name" : "The Moon",
		      "address" : "Space"
		    }
		  }
		]`))

			result := swoop.Get()

			Convey("It should successfully generate a search URL", func() {
				So(result.Success, ShouldBeTrue)
				So(result.ResourceURL, ShouldEqual, "https://swoop.up.co/events")
			})

			swoopData := result.Data.([]swoopevent)
			Convey("It should convert the reponse into SWOOP data", func() {
				So(result.Success, ShouldBeTrue)
				So(result.Data, ShouldNotBeNil)
				So(len(swoopData), ShouldEqual, 1)
				So(swoopData[0].ID, ShouldEqual, 123)
			})

		})

		Convey("When misconfigured", func() {
			swoop := New("badgers", nil)
			result := swoop.Get()

			Convey("It should report failure with an empty URL", func() {
				So(result.Success, ShouldBeFalse)
				So(result.ResourceURL, ShouldEqual, "")
			})
			Convey("It should return nil SWOOP data", func() {
				So(result.Data, ShouldBeNil)
			})

		})
	})
}
