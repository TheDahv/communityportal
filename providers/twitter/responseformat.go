package twitter

type searchResult struct {
	SearchMetadata struct {
		CompletedIn float32 `json:"completed_in"`
		Count       int     `json:"count"`
		MaxID       int     `json:"max_id"`
		MaxIDStr    string  `json:"max_id_str"`
		NextResults string  `json:"next_results"`
		Query       string  `json:"query"`
		RefreshURL  string  `json:"refresh_url"`
		SinceID     int     `json:"since_id"`
		SinceIDStr  string  `json:"since_id_str"`
	} `json:"search_metadata"`
	Statuses []struct {
		ID        int    `json:"id"`
		IDStr     string `json:"id_str"`
		CreatedAt string `json:"created_at"`
		Text      string `json:"text"`
		User      struct {
			Name                string `json:"name"`
			Handle              string `json:"screen_name"`
			ProfileImageURL     string `json:"profile_image_url"`
			ProfileImageURLHttp string `json:"profile_image_url_https"`
			Description         string `json:"description"`
		} `json:"user"`
	} `json:"statuses"`
}
