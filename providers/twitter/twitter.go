package twitter

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/startupweekend/communityportal/providers"
)

const (
	twitterScheme string = "https"
	twitterHost   string = "api.twitter.com"
)

// AuthResponse Encapsulates the auth response from Twitter
type AuthResponse struct {
	TokenType   string `json:"token_type"`
	AccessToken string `json:"access_token"`
}

func getTwitterAuth() (response AuthResponse, err error) {
	// Implement Application-Only Auth workflow documented in
	// https://dev.twitter.com/oauth/application-only

	key := os.Getenv("TWITTER_KEY")
	secret := os.Getenv("TWITTER_SECRET")

	encodedKey := url.QueryEscape(key)
	encodedSecret := url.QueryEscape(secret)

	auth := []byte(encodedKey + ":" + encodedSecret)
	base64Encoded := base64.StdEncoding.EncodeToString(auth)

	authEndpoint := "https://api.twitter.com/oauth2/token"

	client := &http.Client{}
	body := bytes.NewBuffer([]byte("grant_type=client_credentials"))
	req, err := http.NewRequest("POST", authEndpoint, body)
	if err != nil {
		fmt.Println("Twitter Auth request builder failed")
		fmt.Println(err)
		return
	}

	req.Header.Add("Authorization", "Basic "+base64Encoded)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Twitter Auth request returned a failure")
		fmt.Println(err)
		return
	}

	jsonBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading Twitter auth response body")
		fmt.Println(err)
		return
	}

	response = AuthResponse{}
	authParseErr := json.Unmarshal(jsonBody, &response)
	if authParseErr != nil {
		fmt.Println("Error parsing Twitter auth response")
		fmt.Println(authParseErr)
		return
	}

	return
}

// Twitter Represents an authenticated Twitter search
type Twitter struct {
	providers.Base
	SearchTerms []string
	Auth        AuthResponse
}

// New Returns a new Twitter search object
func New(searchTerms []string) Twitter {
	twitter := Twitter{SearchTerms: searchTerms}
	twitter.RemoteHandler = providers.CallRemote

	return twitter
}

// URL Returns the URL of the Twitter search, or an error if the object
// was configured incorrectly
func (p Twitter) URL() (resourceURL string, err error) {
	if len(p.SearchTerms) == 0 {
		err = errors.New("Twitter object requires search terms")
		return
	}

	v := url.Values{}
	v.Add("q", strings.Join(p.SearchTerms, " OR "))

	urlObj := url.URL{
		Scheme:   twitterScheme,
		Host:     twitterHost,
		Path:     "1.1/search/tweets.json",
		RawQuery: v.Encode(),
	}

	resourceURL = urlObj.String()

	return
}

// Get Performs a Twitter search result based on a configured object
func (p Twitter) Get() (result providers.Result) {
	result = providers.Result{
		Success: false,
	}

	// Set up request auth
	if p.Auth.AccessToken == "" {
		auth, err := getTwitterAuth()
		if err != nil {
			fmt.Println("Error authenticating against Twitter")
			fmt.Println(err)
			return
		}

		p.Auth = auth
	}

	searchURL, err := p.URL()
	if err != nil {
		fmt.Println("Error generating URL for Twitter search")
		fmt.Println(err)
		return
	}

	headers := make(map[string]string)
	headers["Authorization"] = "Bearer " + p.Auth.AccessToken

	jsonBody, err := p.RemoteHandler(searchURL, headers)
	if err != nil {
		fmt.Println("Error reading Twitter search response body")
		fmt.Println(err)
		return
	}

	var searchResults searchResult
	parseError := json.Unmarshal(jsonBody, &searchResults)
	if parseError != nil {
		fmt.Println("Error parsing Twitter search results")
		fmt.Println(parseError)
		return
	}

	result.Success = true
	result.ResourceURL = searchURL
	result.Data = searchResults.Statuses

	return
}
