package twitter

import (
	rx "regexp"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestURL(t *testing.T) {

	Convey("When configured with search terms", t, func() {
		searchTerms := []string{"hello", "world"}
		tw := New(searchTerms)

		searchURL, err := tw.URL()
		Convey("It should join the terms with OR", func() {
			results := rx.MustCompile("OR").FindAll([]byte(searchURL), -1)
			So(results, ShouldNotBeNil)
			So(len(results), ShouldEqual, len(searchTerms)-1)
		})

		Convey("It should generate the right URL without error", func() {
			So(err, ShouldBeNil)
		})

	})

	Convey("When configured with empty search terms", t, func() {

		tw := New([]string{})
		searchURL, err := tw.URL()

		Convey("It should return an error", func() {
			So(err, ShouldNotBeNil)
		})

		Convey("It should return a blank URL", func() {
			So(searchURL, ShouldBeBlank)
		})

	})

	Convey("When configured with nil search terms", t, func() {

		tw := New(nil)
		searchURL, err := tw.URL()

		Convey("It should return an error", func() {
			So(err, ShouldNotBeNil)
		})

		Convey("It should return a blank URL", func() {
			So(searchURL, ShouldBeBlank)
		})

	})
}

func TestGet(t *testing.T) {

	Convey("When returning a provider response", t, func() {

		Convey("When configured with search terms", func() {
			tw := New([]string{"hello", "world"})
			tw.Auth.AccessToken = "bingobango"
			tw.RemoteHandler = func(url string, headers map[string]string) ([]byte, error) {
				data := []byte(`{
          "statuses": [
            {
                "created_at": "Sat Mar 07 23:23:53 +0000 2015",
                "id": 574349782379974656,
                "id_str": "574349782379974656",
                "text": "Secrets of Social #Entrepreneurship . http://t.co/NJH1pASn0d #grassroots @GuRuth is my guest.",
                "user": {
                    "description": "A hilarious cartoon duck",
                    "name": "Donald Duck",
                    "profile_image_url": "http://pbs.twimg.com/profile_images/3665270334/bb8103620049919679e256c33745efd5_normal.jpeg",
                    "profile_image_url_https": "https://pbs.twimg.com/profile_images/3665270334/bb8103620049919679e256c33745efd5_normal.jpeg",
                    "screen_name": "DonaldDuck"
                }
            },
            {
              "created_at": "Sat Mar 07 23:23:53 +0000 2015",
              "id": 574349780169682944,
              "id_str": "574349780169682944",
              "text": "RT @ramez: Seattle: @Bruce_Schneier is speaking Monday on how to reclaim our privacy. I'll be introducing him. Come join us: http://t.co/vj\u2026",
              "user": {
                  "description": "Thinks he's the coolest",
                  "name": "Mickey Mouse",
                  "profile_image_url": "http://pbs.twimg.com/profile_images/378800000582428122/4f18a454ff2c6659861f4f4eb5c4458f_normal.jpeg",
                  "profile_image_url_https": "https://pbs.twimg.com/profile_images/378800000582428122/4f18a454ff2c6659861f4f4eb5c4458f_normal.jpeg",
                  "screen_name": "MickeyMouse"
              }
            }
          ]
        }`)

				return data, nil
			}

			result := tw.Get()

			Convey("It should convert the response into Twitter data", func() {
				So(result.Success, ShouldBeTrue)
				So(result.Data, ShouldNotBeNil)
			})

		})

		Convey("When configured without search terms", func() {
			tw := New([]string{})
			tw.Auth.AccessToken = "buffy dislikes vampires"
			result := tw.Get()

			Convey("It should return a failed response", func() {
				So(result.Success, ShouldBeFalse)
				So(result.Data, ShouldBeNil)
			})

		})

	})

}
